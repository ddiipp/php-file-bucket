php-file-bucket
===============
This is a simple php file bucket made fot tannsfaring js and css file easily server to server.

Here is the meaning of all vars.


### $secret_key is your password. That should protect you from hack uploads.

#### Must get querys :
######secret_key
This is the password to edit or create the file.
This is just a string in a php variable.

######file
File is what you are willing to upload. Any utf-8 should be fine.

######type
Type is js or css. This will save the file in following extension

#### Optional querys

###### name
Name is for editing purpose or create a file with specific name. for a `batman.js` you should enter only `batman`



So, you need to query like this 

```
  http://www.example.com/bucket.php?secret_key=reallysecret&file=body,html{padding:0px;margin:0px;}&type=css
```

This will gendrate a file with file content and css type and echo the filename.

And for editing a file you should query

```
  http://www.example.com/bucket.php?secret_key=reallysecret&file=body,html{padding:10px;margin:10px;}&type=css&name=filename_without_extension
```
This will just update the file.

