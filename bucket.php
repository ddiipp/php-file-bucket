<?php


#A very secret key that only you know.
$secret_key = "your secret key";


#we do our action if and only if s(secret key) and f(file) parameter in the get request.
if (isset($_GET['secret_key']) && isset($_GET['file']) && isset($_GET['type'])){
	#now check if secret key matches
	if ($secret_key != $_GET['secret_key']){
		echo "Error 1";
		exit();
	}
	#check file type

	if ($_GET['type'] != "css" && $_GET['type'] != "js" ){
		echo "Error 2";
		exit();
	}

	#Everything seems okey, so lets get the file and save it.

	# Here we have a problem that, we need to create a new file each time, and we dont know 
	#what the name will be, So at this time, thats what we are going to do

	# take the timestamp + a random integer.

	if (isset($_GET['name'])){

		$filename = $_GET['name'] . "." . $_GET['type'];

	}else{

		$date = strtotime("now");
		$randint = rand(1,100);
		$filename = $date . strval($randint) . "." . $_GET['type'];
	}

	$file = fopen($filename,"w");
	fwrite($file,$_GET['file']);
	fclose($file);

	echo $filename;
	exit();
}else{ ?>
<!doctype html>
<html>
<head>
<title> 404 not found. </title>
</head>
<body>
<h1> The thing you are searching thats not here. </h1>
</body>
</html>

<?php }


?>